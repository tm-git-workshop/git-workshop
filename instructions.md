Hi everyone! Glad to have you here in our git workshop. These are our tasks for understanding the file lifecycle in git.


###  Task 1: Add and commit a new folder with 2 txt files inside
- Create a new folder named after yourself (e.g. `bob/`), then create 2 txt files inside this folder, you can name them whatever you like. As an example, see the `sample/` folder and its contents.
- Write a message or a joke in these txt files. **Please add in multiple lines**; we'll need it for Task 2. You can refer to `sample/sample1.txt` as an example.
- Type in `git status`. Note how it shows your newly-created txt files as untracked files.
- Stage the 2 files for committing with `git add <your-folder-name>` (Make sure to replace `<your-folder-name>` accordingly).
- Type in `git status` to verify that your new files have indeed been staged for commit.
- Type in `git commit -m "Add my own txt files"` (or feel free to add your own commit message)
- You should see a success message.
- Type in `git log` to view the history and verify that you see your commit as the latest one (the one on top).
- Congrats! You have just added two new files and commited them to the repository. Git will now remember them and track changes made to these files, as we'll see in Tasks 2 and 3.

### Task 2: Edit a file and commit the change
- Open up one of the txt files you created in Task 1, and edit it. Add new lines, edit the text, your call; just make sure you make some edits.
- Once you're done, type in `git status`. Notice how it says your txt file was modified and not staged for commit?
- Type in `git diff <your-folder-name>/<filename>.txt` (replace with your actual folder and filename). See how git is able to highlight the specific changes you made?
- Once again, type in `git add <your-folder-name>/<filename>.txt` to stage the file for committing.
- Type in `git status` to verify that the file is now marked as staged for commit.
- Type in `git commit -m "Update my txt file"` (or feel free to add your own commit message)
- Type in `git log` and verify that your new commit shows up in the history.
- Congrats! You have just edited a file and committed those changes to your local repository.

### Task 3: Delete a file and commit the change
- Delete one of your txt files by typing in `git rm <your-folder-name>/<filename>.txt` (replace with your actual folder/file names).
- Notice that the file has actually been deleted (it's gone in the file explorer).
- Type in `git status`. Notice git recognizes this file deletion as being staged for commit.
- Type in `git commit -m "Delete a txt file"` (or feel free to write your own commit message).
- Type in `git log` and verify that your latest commit is in the history.
- You have just deleted a file and committed that change to your local repository.


### Task 4: Checkout a previous commit
Now that we have multiple commits, let's try checking out certain versions of our repository. Remember that one purpose of git is so that we have a history of revisions, and that we can re-visit these various versions if we want to.

- Type in `git log`.
- Identify the commit hash of your first commit (the one corresponding to Task 1). A commit hash looks something like this: `61a51a6e075248fc0dcd71a6003bfe9868c2da9f`
- Type in `git checkout <commit-hash>` (replace `<commit-hash>` with your actual commit hash). This changes our repository to reflect what the state was after this commit. Notice how your txt files were restored to their initial state (before the edit in Task 2, and before the deletion in Task 3).
- To return to the latest version, type in `git checkout master` and verify that your latest changes were restored.
- Congrats! You just time-traveled and navigated your local git repository's history.


### Task 5: Pull from a Remote Repository (Bitbucket)
- Type in `git checkout master` just to be sure you are on the latest master branch commit.
- Type in `git pull`. This should pull the latest master changes on Bitbucket and merge them with your local master branch.

### Task 6: Push to a Remote Repository (Bitbucket)
- Type in `git push`
- If you see an error saying your push was rejected, it means someone else has updated the remote master branch ahead of you. You need to `git pull` again to merge those changes with your local branch, and then push afterward. Git does this due to possible conflicts (we'll discuss that later). Just know that to push to a remote branch, you have to pull its latest changes first.
- Note: if everyone is trying to do this at the same time, you might get blocked from pushing multiple times as other people push ahead of you. In this case, you either have to make sure no one else is pushing while you're pushing, or just keep trying until you're lucky enough to push ahead of the others. This is a troublesome scenario, but note that this is very unlikely to happen in a real-world setting, wherein many people are trying to push to one branch at the same time. This is happening only because of our workshop context.

